<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('templat.dashboard');
});

Route::get('/data', function () {
    return view('halaman.data-table');
});


//Route::get('/data-tables',function(){
    //return view('halaman.data-table');
//});

//Route::get('/cast','CastController@index');
//Route::get('/cast/create' ,'CastController@creat');
//Route::post('/cast','CastController@store');

Route::resource('film','FilmController');
Auth::routes();

Route::get('/welcome', 'HomeController@index')->name('home');
