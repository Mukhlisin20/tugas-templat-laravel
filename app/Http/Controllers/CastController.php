<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CastController extends Controller
{
    public function creat()
    {
        return view('data_aktor.form_data');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('casts')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" =>$request["bio"]
        ]);
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('casts')->get();
        return view('data_aktor.table_list', compact('cast'));
    }
}
